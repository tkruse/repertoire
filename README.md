# Songbook

Personal collection of song guitar chords, and a process to render as printable PDF or HTML with Table of Contents.
The song files are `.txt` files in the `txt` folder, the generated PDF can be found inside the `dist` folder.

* Plain PDF download link: https://gitlab.com/tkruse/repertoire/-/raw/master/dist/plain/songbook.pdf
* Experimental output link: https://gitlab.com/tkruse/repertoire/-/raw/master/dist/leadsheets/songbook.pdf

While the source format of the songs is plain txt files like the convention for songs on the internet (for easy adding, editing and sharing), this project also transforms these files into beautifully rendered PDF to allow better readability and printing.
Beautiful means:

* Provide **table of contents**
* Provide alphabetical **song index**
* Latex quality typesetting
* PDF quality fonts and typography and rendering
* Custom fonts
* Songs spread over PDF paper pages
* Layout tries hard to avoid any song flowing over page break

With the experimental parser and latex renderer, additionally:

* Song lyrics are printed in **proportional fonts** (like normal text)
* Chord names are rendered **in color** using musical symbols

Knowledge in the `tex` language is not required to just add and remove songs.

This project is only intended for a personal collection of song chords adapted for personal preference, not a public songbook.
It is provided publicly in this repository because the supporting files (Makefile, latex) are somewhat painful to set up from scratch, so others trying something similar can use this as a starting point.
This project could also be reused for other Collections of raw txt files to be rendered as PDF.

## How to use

To generate a songbook, replace the file in the `txt` folder with your own files.
Some rules to follow:

- the songbook will sort songs from files by filename, so file "a.txt" will come before file "b.txt"
- A song txt file should follow this convention for the title (the leading "#" is important, as well as the " - " separation:
  ```
  # Songtitle1 - Artist1

  contents

  # Songtitle2 - Artist2

  contents
  ```
- Do not start lines with "#" within a song, this is for songtitles only
- Artist cannot be omitted
- A song txt file may contain any number of songs

The file structure of this project is:

```
songbook/
|- Makefile     # Contains the build logic for running make
|- build/       # temporary folder with files generated during build
|- dist/        # final result of generation process
|- transpilers/ # scripts to convert .txt to .tex format
|- tex/         # tex files to render beautiful PDF
|  |- document.tex  # latex root file
|  |- main.tex      # document content structure
|  |- packages.tex  # latex preamble
|- txt/
   |- song1.txt
   |- song2_2col.txt
   |- ...
```

## Prerequisites for building PDF on (Ubuntu) Linux

    # essential helper tools that should already be installed
    sudo apt-get install make python awk find
    # essential libraries to generate PDF
    sudo apt-get install texlive texlive-xetex texlive-music texlive-latex-extra latexmk

    # optional language support (used in package.tex, remove there if not required)
    sudo apt-get install texlive-lang-german texlive-lang-french
    # optional tools for viewing PDF on Ubuntu linux
    sudo apt-get install zathura
    # optional for installing fonts in Ubuntu
    sudo apt-get install typecatcher font-manager

Then run `make` to create/update the pdf inside the `dist` folder.

Other commands:

    make pdf-plain        # simple robust PDF
    make pdf-leadsheets   # experimental PDF for cleanly formatted source files, requires parsimonious
    make clean; make ...  # makes sure to start with no leftover files
    make edit             # also opens pdf viewer
    make                  # same as make pdf

### Installing parsimonous

To parse leadsheets for advanced processing (Pdf-leadsheets), the python parsimonious package also needs to be installed: https://pypi.org/project/parsimonious/.

### Changing the font

The document also uses a custom font, install it or comment out the line in `packages.tex`.

### Building on other operating systems

The essential tools and libraries need to be present, they should be available for any decent OS. The font to be used for the songs can be changed to default (Courier), or other fonts installed on your system. On windows, the makefile might need some changes, or a POSIX environment like Cygwin.

### Build using Docker

If you have a docker environment set up, the following commands should create the PDF files as well:

```
# only first time
docker build -t leadsheets docker

# plain result
./latexdockercmd.sh make
# pretty result
./latexdockercmd.sh make pdf-leadsheets
```

## Technical concept

Requirements considered:

* minimal coding effort, minimal dependencies
* Files should be viewable individually in plain txt format.
* No special character escaping or syntax in source files.
* Multiple songs in one file possible (because is annoying to organize hundreds of files)
* File content optimized for live-usage (not necessarily for learning songs)
* Easy to add/remove songs, minimal formatting effort
* Easy to manage 2 column page layout
* Easy to view/share project online/on mobile device
* Full book should be easily exportable to PDF/HTML with auto-generated Table of Contents and Song Index.
* PDF should be pretty enough to print out on paper as book.
* In book, songs should be roughly ordered, e.g. by genre and artist

Non-requirements:

* Create a beautiful book with chord graphs and pretty tablature.

Process:

* Use filename pattern for metadata and sorting
* Transform txt files into markdown files, preprocess (title and verbatim)
* Transform markdown into latex files using pandoc
* Transform latex files to PDF using latexmk

#### Rejected Alternatives

* **Using Google doc**: At this file size, it is an absolute pain to work with google docs, because it will be very slow
* **Using Office doc**: Hard to make available/viewable online
* **Using one big txt file**: Not possible to reasonably print the songbook on paper, also 2-column layout more painful to manage
* **Using asciiDoctor / Sphinx**: Might be viable, did not try, hard to print
* **Using Javascript**: Coding seems even harder than Makefile and Latex, hard to print
* **Using ultimate-guitar.com**: Viable but website quite distracting, hard to print
* **Using tex files as source**: just too much work, not shareable
* **Establish a markdown format for songs**: This project could serve as starting point, though chordpro exists (but difficult to read as source)

Using Makefile, pandoc and latex is not easy, nor super-clean. But the song source-files remain practically pristine, and the PDF output is just beautiful and highly readable.

* Not using pandoc, not going via markdown
  Currently pandocs contribution in this project seems rather small, defining a latex section and a verbatim environment. However, it does actually a bit more, handling special characters and newlines. Replacing pandoc with simpler tex harnessing would mean handling all such special cases, which would likely require considerable code for parsing txt files. On the other hand that would open up possibiities up much more powerful rendering options, see similar projects below.

#### Similar projects

There are plenty of open songbooks shared online, many also using latex, but few using a simple txt format as source.

* https://gitlab.com/zach-geek/songbook: Ruby toolchain based songbook generation, with additionally melodie rendering, and some metadata capture
* https://github.com/remcorakers/songbook much simpler approach, using filename as section title
* https://github.com/janecekt/guitar-song-book-editor some more export options, based on chordpro format
* https://github.com/mjirik/sobo python coding

### Resources

This document explains the diverse possibilities for rendering music using Tex:
http://tug.ctan.org/info/latex4musicians/latex4musicians.pdf

Music can be printed for various purposes:

* to define a song
* to learn/teach songs
* to produce music as a band or a group (e.g. at campfires, in church)
* to sell a songbook

As such, the level of detail needed in the print varies a lot, possible elements are:

* Song historic metadata (title, artist, writer, Copyright, release year, album, genre, description, comments, covers, alternative titles, ...)
* Song technical metadata (Key, instruments, ...)
* Direct song data (melody, rhythm, lyrics, voices, instruments, ...)
* Assistive data (chords, lyrics combined with chords or melody, with multiple voices, ...)
* Instrument-specific data (guitar tablature, chord diagrams, strumming pattern, non-standard tuning, Capo position, effects, ...)
* Customization and i18n (transcode, transpose, translate, patches, ...)

And a given desired print output can combine any of the above elements in diverse order and presentation. Direct song data is most likely subject to copyright law.

* *The "chordpro" format* is well-established for simple sheets for guitars and similar instruments.
* *ABC notation* is often used for melodies.
* *musixml* can be used to share extensive data aspects about a song between programs.
* *MIDI* can be used to reproduce songs using virtual instruments.
* *lilypond.org* is a free file format for musc sheet rendering
* *Ultimate-guitar.com* is the most prominent among several online database storing (guitar) sheets in a semi-standard way for direct use.
* *TablEdit* is a venerable licensed program for sheet music creation

Just rendering text and chords in a useful way has several challenges:

* lines can become longer than a paper width, number of lines can become longer than paper length
  * chorus repetitions can be avoided
  * chords repeating in stanzas can be removed from all but the first stanza
  * other repetitions can be just indicated by "3x"
  * lyrics can be rendered in multiple columns
    * Multiple columns might split verse blocks, but shoud not
    * not all lyrics look good in 2-column shape
  * font size can be reduced
  * rhythm data can be added by special rendering in lyrics (underline letters)
  * In tablature, only few words of the lyrics might be shown
  * Instead of Monospace font, adjusted proportional font could be used
  * Tablature could be rendered graphically instead of the monospace text lines
  * Section title "Verse x, chorus" could be rendered in the left margin
* one word or syllable may span several chords
* the link between lyrics and rhythm may not be evident
* Tablature carries less information about melody than classical notation
* some chords might best be played in different spots on the fretboard than standard location
* Different paper formats used in different places around the world
* Different languages use non-ASCII symbols (opening a pandoras box of encoding issues)
* tabs and spaces mix, file encodings

Based on ultimate-guitar.com, the most commonly elements for a guitar sheet are, in the order of relevance:

* Title and artist
* Lyrics with chords rendered in a line above
* Capo position
* Chords recognized to allow transcoding / transposing
* Separate semi-formal song parts, e.g. Intro, Verse, Chorus, Outro, Interlude
* Strumming patterns
* Tuning
* Playing hints (Special chords, )
* Tablature for melodies

The textual format that's somewhat standardized may look like this:

```
Title - Artist (different formats)
Diverse comments or informal metadata

Capo: II

[Riff]

e|----------------|
B|----------------|
G|----------------|
D|--------------7~|
A|--5b-7--5/7-----|
E|----------------|

[Intro]
(4/4)
| Eb |   |   | F |

[Verse 1]
(slowly)
Am  .          .         .        Am  .   Em Em/G Am
    Ain't no sunshine when she's gone.

[Chorus]
Am  (silent)                  Am        Em Em/G Am
It's not warm when she's away.

[Outro]
G  Am  C  G  x2  (end on single strum)

Chords:
F          xx3211
C/E        xx2010
```

This format is not optimized for paper-printing (on a single or double page), instead it seems optimized for learning/screen-printing with autoscroll (Not optimizing for paper-print might be intentional to discourage printing and increase site visits).
This mish-mash of examples shows that a parser has to deal with a lot of irregularities that sheet music authors add to their files.
Ultimate-guitar.com only seems to attemt to recognize lines that are mostly chords and process those chords for transposing.
However, a parser for private purposes can be stricter than ultimate guitar in the required format for text files, by being stricter more optimizations can be automated for paper-print page rendering.

A general problem with this format over the chordpro format is that whitespace in the lyrics could either mean a pause in the lyrics, or just spacing to fit chords above the line:
```
C                Cmaj7 F
Imagine there’s no    heaven
```
The best that can be done about it is to just assume the output should be rendered as the input, so the whitespace should remain in the rendering even when not necessary.

## TODO

* Fill missing songs
* Use subfolders for book structure
* add latex chapters for subfolders
* Provide github actions based workflow to generate PDF from source files
