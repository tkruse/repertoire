# Bella Ciao - Traditional

> Alternative chord ascension: Am-Dm-E7:Bm-Em-F#7:Cm-Fm-G7

Am

        Am
Una mattina    mi son svegliato,

O bella, ciao! Bella, ciao! Bella,
 Am7
ciao, ciao, ciao!
       Dm                 Am
Una mattina    mi son svegliato
        E7          Am
e ho trovato l'invasor.


        Am                 .
O partigiano,     portami via,

O bella, ciao! Bella, ciao! Bella,
 Am7
ciao, ciao, ciao!
        Dm              Am
O partigiano,   portami via,
        E7         Am
ché mi sento di morir.


Am
E se io muoio    da partigiano,

O bella, ciao! Bella, ciao! Bella,
 Am7
ciao, ciao, ciao!
         Dm               Am
E se io muoio    da partigiano,
         E7                    Am
tu mi    de - vi   se - ppe - llir.


       Am
Seppellire     lassù in montagna,

O bella, ciao! Bella, ciao! Bella,
 Am7
ciao, ciao, ciao!
        Dm                   Am
E seppellire     lassù in montagna
        E7                 Am
Sotto l'ombra  di'un  bel fior.


        Am
E le genti    che passeranno

O bella, ciao! Bella, ciao! Bella,
 Am7
ciao, ciao, ciao!
      Dm                Am
E le genti    che passeranno
      E7                   Am
Ti diranno   "Che   bel   fior!"


    Am
"È questo il fiore    del partigiano",

O bella, ciao! Bella, ciao! Bella,
 Am7
ciao, ciao, ciao!
             Dm                 Am
"È questo il fiore    del partigiano
      E7               Am
morto per    la   libertà!"
