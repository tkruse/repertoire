#! python

"""
Convert txt to latex such that all instances of

    # The Title - The Artist

     A     C    G
    Some great song

become

    \hypertarget{the-title---the-artist}{%
    \section{The Title - The Artist}\index{The Title}\label{the-title---the-artist}}

    \begin{verbatim}
     A     C    G
    Some great song
    \end{verbatim}
"""


from argparse import ArgumentParser
import sys
import json
import logging
import re
import textwrap
import unicodedata
from string import Template

logger = logging.getLogger('main')

TITLE_PATTERN = re.compile(
    r"\n*^# ([^$]+?) - ([^$-]+?)$\n*",
    flags=re.MULTILINE)

SONG_BODY_TEX_PATTERN = re.compile(
    r"\\begin{verbatim}(.*?)\\end{verbatim}",
    flags=re.MULTILINE+re.DOTALL)

SONG_TITLE_TEMPLATE =  Template(r"""
\hypertarget{$title_key---$artist_key}{%
\section{$title - $artist}\index{$title_index}\label{$title_key---$artist_key}}

\begin{verbatim}
""")

VERBATIM_END = "\\end{verbatim}\n\n"

def sanitize_latex(data):
    return data.replace("&", "\&").strip()

def to_key(data):
    return (str(unicodedata
                .normalize('NFKD', data
                           .replace("-", "")
                )
                .encode('ASCII', 'ignore')
                .decode("utf-8"))
            .lower()
            .replace(" ", "-")
            .replace(".", "")
            .replace(",", "")
            .replace("!", "")
            .replace("\\&", "and")
            .replace("''", "")
            .replace("\"", "")
    )

def replace_title_match_with_tex(match):
    title= match.group(1).strip()
    artist = match.group(2).strip()
    result = SONG_TITLE_TEMPLATE.substitute(
        title=title,
        artist=artist,
        title_key=to_key(title),
        artist_key=to_key(artist),
        title_index=to_key(title) + "@" + title
    )
    return result

def convert_song(filename, content):
    result = TITLE_PATTERN.sub(replace_title_match_with_tex, sanitize_latex(content))
    result += VERBATIM_END
    return result

# multicol is annoying for short songs, try to limit to when it's possible and may fill a whole page

MULTICOL_MIN_LINES = 60
MULTICOL_MAX_LINELENGTH = 56

def replace_body_tex_match_with_multicol(match):
    body = match.group(1)
    if (body.count('\n') > MULTICOL_MIN_LINES
        and len([l for l in body.split("\n")
                 if not l.startswith(">") and len(l) > MULTICOL_MAX_LINELENGTH]) == 0):
        return (
            textwrap.dedent(r"""
              \begin{multicols}{2}
              \begin{verbatim}""")
            + body
            + textwrap.dedent(r"""
              \end{verbatim}
              \end{multicols}
            """))
    elif (body.count('\n') > MULTICOL_MIN_LINES * 2
          and len([l for l in body.split("\n")
                 if not l.startswith(">") and len(l) > MULTICOL_MAX_LINELENGTH + 5]) == 0):
        return (
            textwrap.dedent(r"""
              \begin{multicols}{2}
              \begin{Verbatim}[fontsize=\footnotesize]""")
            + body
            + textwrap.dedent(r"""
              \end{Verbatim}
              \end{multicols}
            """))
    # nothing to do
    return match.group(0)

def convert_multicol(song):
    newsong = SONG_BODY_TEX_PATTERN.sub(replace_body_tex_match_with_multicol, song)
    if song != newsong:
        newsong = "\\clearpage{}\n" + newsong
    return newsong

################## Command line

def print_songs(songs):
    print(songs)

def split_songfile(content):
    '''Retun a list of strings, each one song, identified by title line'''
    return ["# " + r for r in re.split(
            "^# ", content, flags = re.MULTILINE + re.DOTALL) if len(r.strip()) > 0]

def main_with_argv(argv):  # Ignore VultureBear
    """CLI entry point."""
    args = argv[1:]
    parser = get_args_parser()  # type : ArgumentParser
    args = parser.parse_args(args)

    args = evaluate_options(args)

    logger.debug(args.path)
    tex = ""
    if len(args.path) == 0:
        raise ValueError("Must provide filename")
    for filename in args.path:
        content = load_file(filename)
        songs = split_songfile(content)
        for song in songs:
            songtex = convert_song(filename, song)
            tex += convert_multicol(songtex)
    print_songs(tex)
    return 0

def get_args_parser(description=''):  # type: (str) -> ArgumentParser
    """Add all options and arguments."""
    parser = ArgumentParser(description=description)
    parser.add_argument('path', nargs='*', help='a filename to convert')
    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        default=False,
        help="verbose output.",
        action="store_true")
    return parser


def evaluate_options(args):
    """Validate options, set defaults."""
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
        logging.debug("Verbose logging on")
    return args

def load_file(ofilename):
    text = None
    with open(ofilename, 'rb') as f:
        textraw = f.read()
    try:
        text = textraw.decode('utf-8')
    except:
        # TODO: Use chardet, unittest
        text= textraw.decode('cp1250')
    return text


def main():  # Ignore VultureBear
    main_with_argv(sys.argv)

if __name__ == "__main__":
    main()
