# Plain style: This transforms song txt using a local python script:
#
# The songs will be rendered with a monospaced font.
# For 2-column layout, guess intention.

# Because this treats each song (except title) as a single verbatim block, the rendering output is very predictable (but a bit boring).


# TODO: multicol still requires weird block of empty lines to align second column


# make magic not needed
export MAKEFLAGS += --no-builtin-rules
.SUFFIXES:


#################### Other custom preprocessing

export CONVERTER_PY = $(VARIANTS_DIR)/plain/python/convert.py

.PRECIOUS: $(BUILD_DIR)/plain/tex/%.tex
$(BUILD_DIR)/plain/tex/%.tex: $(TXT_DIR)/%.txt $(CONVERTER_PY)
	python $(CONVERTER_PY) $< > $@
