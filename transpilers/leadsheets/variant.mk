# leadsheets style: This uses a custom python parser and a custom text emitter to create tex files with proportional fonts.
#
# To setup custom parser
# Use python version 3.7.5 or newer
# Install the requirements from python/requirements.txt
#
# Suggested steps:
# pyenv virtualenv 3.7.5 songbook
# pyenv activate songbook
# pip install -r transpilers/leadsheets/python/requirements.txt
#
# * Inserts chords above text, allowing proportional font
# * For 2-column layout, uses heuristics
# * guesses 2-col songs based on proportional font

# make magic not needed
export MAKEFLAGS += --no-builtin-rules
.SUFFIXES:

PYTHON_DIR = $(VARIANTS_DIR)/leadsheets/python
CONVERTER_PYS := $(wildcard $(PYTHON_DIR)/songmarkdown/*.py)

.PRECIOUS: $(BUILD_DIR)/leadsheets/tex/%.tex
$(BUILD_DIR)/leadsheets/tex/%.tex: $(TXT_DIR)/%.txt $(CONVERTER_PYS)
	cd $(PYTHON_DIR); PYTHONPATH=$(PYTHON_DIR) python3 $(PYTHON_DIR)/songmarkdown/songmarkdown_main.py $< --format=leadsheet > $@
