# Experimental leadsheets latex transpiler

This transpiler attempts to first read the txt file and understand what line means what, and then use complex latex commands to render leadsheets in a more beautiful way than the default plain transpiler.

More beautiful means:

* Song lyrics are printed in proportional fonts (like normal text)
* Chord names are rendered using musical symbols

This may increase the readability of leadsheets, and make them prettier to print when distributing to groups for singing along.

## prerequisites

* Python > 3.7
* Python `parsimonous` package installed for parser generation

## Song format rules

In order to work, several rules must be followed to allow understanding the txt file. Basically a song must look like this:

```
# Song title - Artist name

> Optional comments about how to play song

Capo III

Intro: Dm Am Dm Am Gm Am Dm-Am-Dm

|--------------------------------------------------|
|-----0-1------------------------------------------|
|-------------------------------0-2----------------|
|---2-------(2)-(0h2p0)---------------(0)----------|
|-0---------------------(3)---3-----------(3)------|
|---------------------------1----------------------|

[Optional section title]
Dm    (Quiet picking)           A
  No more will my green sea go turn a deeper blue.
```

* The title and artist line must must start with a hash `# ` and separate title and artist with a dash ` - `, and there should be no additional dashes in either thte titel or the name. it must be followed by an empty line.
* Comments can be added at the top starting the line with `> `
* Optionally a Capo information can be added, separated by empty lines.
* Else the song should contain of sections separated by empty lines, which can be:
** just a single line of chords `A C G D`
** tablature lines without chords or lyrics
** Multiple lyrics lines, each of which may optionally be preceded by a line of chords
*** A line of chords should have only chords, some separators `[0-9,.|]`, and comments in parentheses
** lyric lines should not have any unusual symbols

Those rules can feel restrictive, and when the process fails the error message might not be very helpful in this prototype, soin case of errors it's best to remove most of the song and try to see which part violates any of the rules above.
