"""
formats a Song objects into complex latex.
"""

import logging
import re
import textwrap
import unicodedata

logger = logging.getLogger('latex_formatter')

def to_key(data):
    """sanitizes wild text to be used as latex key"""
    return (str(unicodedata
                .normalize('NFKD', data
                           .replace("-", "")
                )
                .encode('ASCII', 'ignore')
                .decode("utf-8"))
            .lower()
            .replace(" ", "-")
            .replace(".", "")
            .replace(",", "")
            .replace("!", "")
            .replace("\\&", "and")
            .replace("''", "")
            .replace("\"", "")
    )

def render_title(song, options=""):
    capo = (", capo={%s}" % toArabic(song.capo) if song.capo is not None and len(song.capo) > 0 else '')
    ## properly sort titles starting with non-ascii letters
    index_key = to_key(song.title) + "@" + song.title
    song_result = (r"\begin{song}%s{title={%s}, interpret={%s}%s}\index{%s}"
                   % (options, song.title, song.artist, capo, index_key))
    song_result = song_result.replace("&", r"\&") + "\n"
    song_result += "\n"
    for descline in song.description:
        song_result += (
            r"\textmono{\texttt{"
            + (descline
                .replace("\\", "\\\\")
                .replace("^", r"\^{}")
                .replace("_", r"\_")
                .replace("|", r"\textbar{}")
                .replace("$", r"\$")
                .replace(" ", r"\phantom{ }")
              )
            + r"}}\newline{}" + "\n"
            )
    if len(song.description) > 0:
        song_result += r"\newline{}" + "\n"

    return song_result

def format_leadsheet(songs):
    result = ""
    for song in songs:
        if song.errors == []:
            result += render_title(song, "[remember-chords=false,smash-chords=true]")
            result += format_leadsheet_sections(song.sections)
            result += (r"\end{song}" + "\n\n")
        else:
            # Verbatim does not work within song environment :-(
            result += render_title(song)
            result += (r"\end{song}" + "\n\n")
            result += r"\begin{verbatim}" + "\n" + song.raw + "\n" + r"\end{verbatim}" + "\n"

    return result

def toArabic(num):
    if re.match(r"^[0-9]*$", num):
        return num
    roman_numerals = {'I':1, 'V':5, 'X':10, 'L':50, 'C':100, 'D':500, 'M':1000}
    result = 0
    for i, c in enumerate(num):
        if (i + 1) == len(num) or roman_numerals[c] >= roman_numerals[num[i+1]]:
            result += roman_numerals[c]
        else:
            result -= roman_numerals[c]
    return result

def textsize(text):
    """
    Calculate approximate proportional width of text.
    This can only be a heuristic about how much space a line will take,
    latex will dynamically change letter spacing.
    """
    sum = 0.0
    for c in text:
        # the numbers are not well-tested, they obviously depend on the font
        if c.lower() in [' ', ',', '.', '\'', 'i', 'f', 'l', 'j', 't']:
            sum += 0.5
        elif c.isupper() or c.lower() in ['m', 'w']:
            sum += 1.2
        else:
            sum += 1
    return sum

def format_leadsheet_sections(sections):
    result = ""
    results = []
    songlength = 2
    for section in sections:
        songlength += 1
        multicol = True
        for line in section.lines:
            if len(line.tablature) > 0:
                for tablature_line in line.tablature:
                    songlength += 1
                    if textsize(tablature_line) > 40:
                        multicol = False
                songlength += 1
            # TODO: count overhang chords
            if textsize(line.main_lyrics) > 42:
                multicol = False
            if textsize(line.main_lyrics) > 0:
                songlength += 1
            if textsize(line.chords) > 0:
                songlength += 1
        results.append((format_leadsheet_section(section), multicol))
    # split columns in singlecolumns (first) and multicolumns (rest)
    singlecols = []
    multicols = []
    # multicols only worth it for long songs, partial pagefill often annoying
    if songlength > 50 and len(results) > 4 or songlength > 60:
        # if song is very long, must be multicol in any case (TODO: override)
        if songlength > 60 or all([m for (_, m) in results]):
            # all multicols
            multicols = [t for (t, _) in results]
        elif len(results) > 4 and all([m for (_, m) in results[1:]]):
            # all except first are multicols
            singlecols = [results[0][0]]
            multicols = [t for (t, _) in results[1:]]
        elif len(results) > 4 and all([m for (_, m) in results[2:]]):
            # all except first two are multicols
            singlecols = [t for (t, _) in results[:2]]
            multicols = [t for (t, _) in results[2:]]
        else:
            # all singlecol
            singlecols = [t for (t, _) in results]
    else:
        # all singlecol
        singlecols = [t for (t, _) in results]

    if songlength > 120:
        result += r'\footnotesize{'
    result += '\n\n\\bigskip{}\n\n'.join(singlecols)
    if len(multicols) > 0:
        if len(singlecols) > 0:
            result += "\n\n\\bigskip{}" + "\n"
        result += r"\begin{multicols}{2}" + "\n"
        result += r"  \raggedcolumns{}" + "\n"
        result += r"  \interlinepenalty=10000" + "\n"
        for i, column in enumerate(multicols):
            if i > 0:
                result += "\n\n    \\bigskip{}\n\n"
            result += column
        result += r"\end{multicols}" + "\n"
    if songlength > 120:
        result += '}'
    return result

def format_leadsheet_section(section):
    name = "verse"
    result = ""
    if len(section.lines) > 0:
        if section.name is not None:
            if section.name.lower().startswith("intro"):
                name = "intro"
            if section.name.lower().startswith("outro"):
                name = "outro"
            if section.name.lower().startswith("chorus"):
                name = "chorus"
    # result += (r"  \begin{%s}" % name) + "\n"
    if len(section.lines) > 0:
        for i, line in enumerate(section.lines):
            if i > 0:
                # for lines without lyrics, end paragraph, add empty line
                if len(line.main_lyrics) == 0:
                    result += "\n\n\\bigskip{}\n\n"
                else:
                    # use \\* to prevent column split
                    result += "\\\\* \n"
            result += format_leadsheet_line(line)
        result += "\n"
    else:
        result += section.name + "\n\n"
    # result += (r"  \end{%s}" % name) + "\n\n"
    return result

def escaped(text):
    return (text
                .replace("\\", r"\textbackslash{}")
                .replace(" ", r"\blank{}")
                .replace("&", r"\&")
                .replace("_", r"\_")
                .replace("-", r"\textendash{}")
                .replace("~", r"\textasciitilde{}")
                .replace("|", r"\rule[-0.4ex]{0.2ex}{1.2em}")
                )

def format_without_lyrics(line):
    texline = ""
    if len(line.chordpro) > 0:
        texline = (line.chordpro
                   .replace("{", "")
                   .replace("}", "")
                   .replace("\\", r"\textbackslash{}")
                   .replace("&", r"\&")
                   .replace("|", r"\textbar{}")
                   .replace("~", r"\textasciitilde{}")
                   .replace("[", r"\cchord{")
                   .replace("]", "}")
        )
        if len(line.tablature) > 0:
            texline += r"  \\" + "\n"
    if len(line.tablature) > 0:
        # use \\* to prevent column split
        texline += "\\\\*\n".join([r"\textmono{\texttt{"
                                   + escaped(tabline)
                                   + "}}" for tabline in line.tablature])
    return texline

def format_leadsheet_line(line):
    if line.chordpro is None:
        line.chordpro = merge_line(line.chords, line.main_lyrics)

    # single chord or tabulature line
    if len(line.main_lyrics) == 0:
        texline = format_without_lyrics(line)
    else:
        chordpro = line.chordpro
        texline = "    "
        pos = 0
        spacing_required = False
        while pos < len(chordpro):
            # chordpro line has chords in [], comments in {}, other symbols as-is
            if chordpro[pos] == '[':
                if spacing_required is True:
                    texline += escaped(" ")
                # insert chords rendered using \cchord
                pos += 1 # skip open bracket
                texline += r"\upchord{"
                # count how many characters on lyric line to include
                chordtext = ""
                while pos < len(chordpro) and chordpro[pos] != ']':
                    texline += chordpro[pos].replace("\\", r"\textbackslash{}")
                    chordtext += chordpro[pos]
                    pos += 1
                texline += "}{"
                pos += 1 # skip close bracket
                # don't go beyond word boundaries
                chordtext_size = textsize(chordtext) + 0.5
                swallowed = ""
                non_white_swallowed = False
                while pos < len(chordpro) and textsize(swallowed) < chordtext_size and chordpro[pos] not in ['[', '{', " "]:
                    texline += escaped(chordpro[pos])
                    swallowed += chordpro[pos]
                    pos += 1
                    non_white_swallowed = True
                while pos < len(chordpro) and textsize(swallowed) < chordtext_size and chordpro[pos] in [' ']:
                    texline += escaped(chordpro[pos])
                    swallowed += chordpro[pos]
                    pos += 1
                if non_white_swallowed is False:
                    while pos < len(chordpro) and textsize(swallowed) < chordtext_size and chordpro[pos] not in ['[', '{', " "]:
                        texline += escaped(chordpro[pos])
                        swallowed += chordpro[pos]
                        pos += 1
                while textsize(swallowed) < chordtext_size:
                    texline += escaped(" ")
                    swallowed += " "
                texline += "}"
                spacing_required = True
            elif chordpro[pos] == '{':
                if spacing_required is True:
                    texline += escaped(" ")
                pos += 1 # skip bracket
                texline += r"\textoverset{\textit{"
                comment_text = ""
                while pos < len(chordpro) and chordpro[pos] != '}':
                    texline += escaped(chordpro[pos])
                    pos += 1
                    comment_text += chordpro[pos]
                texline += "}}{"
                pos += 1 # skip bracket
                comment_text_size = textsize(comment_text)
                swallowed = ""
                non_white_swallowed = False
                while pos < len(chordpro) and textsize(swallowed) < comment_text_size and chordpro[pos] not in ['[', '{']:
                    texline += escaped(chordpro[pos])
                    swallowed += chordpro[pos]
                    pos += 1
                while pos < len(chordpro) and textsize(swallowed) < comment_text_size and chordpro[pos] in [' ']:
                    texline += escaped(chordpro[pos])
                    swallowed += chordpro[pos]
                    pos += 1
                if non_white_swallowed is False:
                    while pos < len(chordpro) and textsize(swallowed) < comment_text_size and chordpro[pos] not in ['[', '{']:
                        texline += escaped(chordpro[pos])
                        swallowed += chordpro[pos]
                        pos += 1
                while textsize(swallowed) < comment_text_size:
                    texline += escaped(" ")
                    swallowed += " "
                texline += "}"
                spacing_required = True
            else:
                spacing_required = False
                texline += escaped(chordpro[pos])
                pos += 1
    return texline

def merge_line(chordline, lyricline):
    if chordline is None or len(chordline) == 0:
        return lyricline
    if len(lyricline) == 0:
        return chordline
    if len(chordline) == 0:
        return lyricline
    result = ''
    lyricspos = 0
    chordspos = 0
    while chordspos < len(chordline) or lyricspos < len(lyricline):
        if chordspos < len(chordline) and chordline[chordspos] in ['[', '{']:
            copy_pos = chordspos
            while copy_pos < len(chordline) and chordline[copy_pos] not in [']','}']:
                result += chordline[copy_pos]
                copy_pos += 1
            result += chordline[copy_pos]
            # add similar number of letters from lyricsline if any
            added_text = False
            for catchup in range(lyricspos, lyricspos + (copy_pos - chordspos - 1)):
                if catchup < len(lyricline):
                    added_text = True
                    result += lyricline[catchup]
                    lyricspos += 1
            # spacing between chords
            if not added_text:
                result += " "
            chordspos = copy_pos + 1
        else:
            if lyricspos < len(lyricline):
                result += lyricline[lyricspos]
                lyricspos += 1
            chordspos += 1
    logger.info(result)
    return result
