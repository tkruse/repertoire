'''
Songmarkdown parser
This reads a string and produces a song object with text tokens from the input.
'''

# created using library parsimonious==0.8.1
from parsimonious.grammar import Grammar, NodeVisitor # type: ignore

import logging
import json
import re
from typing import List, Optional, Any

from dataclasses import dataclass, field

logger = logging.getLogger('parser')

# TODO
# Provide grammar for just one song
# fix pytest string deprecation warnings
# support/disambiguate multivoice textline
# parse Optional chords
# parse strumming pattern
# parse sectionref (Chorus) comments
# special parsing of meta-comments
# meta-section with Key-value format
# parse chordpro format
# parse rendering directives
# parse chord definition like "C  x32010"
# Customizable grammar (e.g. title separator, chords regex)

## Reminder of how the grammar and visitor interact:
# node will have children as element on the right
# in alternatives, left takes priority over right
# parentheses cause nesting of (anonymous) nodes
# & before node requires the node, but does not consume it
# nodes should consume empy lines in the beginning, but just request at the end
# visitor method will be called after all children have been visited
grammar = Grammar("""
songfile         = (emptyline * song) + emptyline*
song             = title_and_artist spaces_newline &emptyline description_section* capoline_opt body

title_and_artist = titlemarker_opt title title_separator artist
title_separator  = " " spaces_opt "- " spaces_opt
titlemarker_opt  = ~"#+[\s]+"
title            = ~"[\w0-9]([\w',.!?]-|[\w',.!?/ ()]+)*[\w0-9'.!?)]"
artist           = ~"[\w0-9][\w&',.!? ()/]*[\w0-9'.!?)]"

capoline_opt     = (emptyline* spaces_opt capolabel caponumber "."?)?
capolabel        = ~"capo[:]?[ ]+"i
caponumber       = ~"[0-9IVX]+"

body             = (description_section / tablature_section / short_section / section / sectionref) +
short_section    = emptyline+ (sectiondef ":")? chordline &emptyline
section          = emptyline+ (sectionheader)? line +
sectionref       = emptyline+ sectionheader !words
sectionheader    = "[" sectiondef ":"? "]" spaces_newline
sectiondef       = ~"[A-Z][a-z]+\s*([0-9]*[x]?)"
line             = chordline? textline

description_section = emptyline* (description_line+)
description_line = ">" " "? description
description      = ~"[ ]*.*" spaces_newline


tablature_section = emptyline+ tablature_line (emptyline tablature_line) *
tablature_line    = ((spaces_opt tablature_part_line spaces_newline) / chordline) (spaces_opt tablature_part_line spaces_newline)+
tablature_part_line   = ~"([a-hA-H][#b]?)?\\|-[0-9hp()\\|\\-~/\\\\\]+-[|]?"

chordline        = chordline_fragment + spaces_newline
chordline_fragment = chord_spaces_opt ( chord / chord_separator / "-" / chord_comment)
chord_separator  = ~"[0-9,.|\\-x]+"
chord_comment    = "(" ~"[^)\\n]+" ")"
chord            = ~"[a-hA-H][b#]*(m|mi|ma|M)?[ajdsu#()245679*]*([/\\\\\][A-H245679][b#]?)?"
chord_spaces_opt = ~"[ ]*"

textline         = words newline
words            = ~"[ -]*[\\w(\\'\\"][ \\w:&#/.,;!?()\\'\\"\\-]*"

emptyline        = (spaces_opt newline)
spaces           = ~"[ ]+"
spaces_newline   = spaces_opt newline
spaces_opt       = ~"[ ]*"
newlines         = newline +
newline          = ~"[\\n]"
""")


@dataclass
class SongLine:
    main_lyrics: str = ""
    other_lyrics: List[str] = field(default_factory=list)
    chords: str = ""
    chordpro: Optional[str] = None
    tablature: List[str] = field(default_factory=list)

@dataclass
class SongSection:
    name: str = ""
    lines: List[SongLine] = field(default_factory=list)

@dataclass
class Song:
    title: str = ""
    artist: str = ""
    capo: Optional[None] = None
    sections: List[SongSection] = field(default_factory=list)
    description: List[str] = field(default_factory=list)
    raw: str = ""
    errors: List[Any] = field(default_factory=list)


TITLE_PATTERN = re.compile(
    r"\n*^# ([^$]+?) - ([^$-]+?)$\n*",
    flags=re.MULTILINE)

class SongMarkdownVisitor(NodeVisitor):

    def __init__(self):
        self._songs = []
        self.reset_song()

    def reset_song(self):
        self._verse_counter = 1
        self._current_song = Song()
        self.reset_section()

    def reset_section(self):
        self._current_section = SongSection()
        self.reset_line()

    def reset_line(self):
        self._current_line = SongLine()
        self.reset_chordline()

    def reset_chordline(self):
        self._chordline = ""

    def get_songs(self):
        return self._songs

    def visit_song(self, node, visited_children):
        self._songs.append(self._current_song)
        logger.debug('Song parsed "%s"' % self._current_song.title)
        self.reset_song()

    def visit_title_and_artist(self, node, visited_children):
        logger.debug('Title and artist "%s"' % node.text)
        _, title, _, artist, *_ = visited_children
        self._current_song.title = title.text
        self._current_song.artist = artist.text

    def visit_caponumber(self, node, visited_children):
        logger.debug('Capo "%s"' % node.text)
        self._current_song.capo = node.text

    def visit_description(self, node, visited_children):
        logger.debug('Description %s' % node.text)
        self._current_song.description.append(visited_children[0].text)

    def visit_section(self, node, visited_children):
        self._current_song.sections.append(self._current_section)
        self.reset_section()

    def visit_short_section(self, node, visited_children):
        self._current_section.lines.append(self._current_line)
        self._current_song.sections.append(self._current_section)
        self.reset_section()

    def visit_sectionref(self, node, visited_children):
        self._current_song.sections.append(self._current_section)
        self.reset_section()

    def visit_line(self, node, visited_children):
        self._current_section.lines.append(self._current_line)
        self.reset_line()

    def visit_chordline(self, node, visited_children):
        if len(visited_children) > 0:
            # TODO: return list of fragments, cleanup newline
            self._current_line.chords = self._chordline

    def visit_chord_spaces_opt(self, node, visited_children):
        self._chordline += node.text

    def visit_chord_separator(self, node, visited_children):
        self._chordline += "{" + node.text + "}"

    def visit_chord_comment(self, node, visited_children):
        self._chordline += "{" + node.text + "}"

    def visit_chord(self, node, visited_children):
        self._chordline += "[" + node.text + "]"

    def visit_textline(self, node, visited_children):
        if self._current_line.main_lyrics == "":
            self._current_line.main_lyrics = visited_children[0]
        else:
            self._current_line.other_lyrics.append(visited_children[0])

    def visit_tablature_section(self, node, visited_children):
        self._current_song.sections.append(self._current_section)
        self.reset_section()

    def visit_tablature_line(self, node, visited_children):
        self._current_section.lines.append(self._current_line)
        self.reset_line()

    def visit_tablature_part_line(self, node, visited_children):
        self._current_line.tablature.append(node.text)

    def visit_words(self, node, visited_children):
        return node.text

    def visit_sectiondef(self, node, visited_children):
        self._current_section.name = node.text

    def generic_visit(self, node, visited_children):
        """ The generic visit method. """
        return visited_children or node

def split_songfile(content):
    '''Return a list of strings, each one song, identified by title line'''
    return ["# " + r.strip() + "\n\n" for r in re.split(
            "^# ", content, flags = re.MULTILINE + re.DOTALL) if len(r.strip()) > 0]

def create_fallback(content, filename, error):
    match = TITLE_PATTERN.match(content)
    song = Song()
    song.title = match.group(1) if match is not None else ""
    song.artist = match.group(2) if match is not None else ""
    song.raw = content.split("\n", 1)[1].strip()
    error_msg = 'Could not parse ' + song.title + " from " + filename + ": " + str(error)
    song.errors = [error_msg]
    return song

def parse_songs(filename, content):
    logger.debug("parsing %s" % filename)

    # unusual quotes make the parser break
    clean_content = (content
        .replace("’", "'")
        .replace("‘", "'")
        .replace("“", '"')
        .replace("”", '"')
        .replace("«", '"')
        .replace("»", '"')
        .replace("…", '...')
    )
    check_bad_chars(filename, clean_content)

    single_song_contents = split_songfile(clean_content)

    songs = []
    for single_song_content in single_song_contents:
        try:
            tree = grammar.parse(single_song_content)
            smv = SongMarkdownVisitor()
            smv.visit(tree)
            # should be only one song
            parsed_songs = smv.get_songs()
            songs.extend(parsed_songs)
        except Exception as e:
            fallback_song = create_fallback(single_song_content, filename, e)
            logger.warn(str(fallback_song.errors), exc_info=e)
            songs.append(fallback_song)

    return songs

def check_bad_chars(filename, content):
    invalid_chars = '\t\f'
    bad_chars = set(invalid_chars).intersection(content)
    if bad_chars:
        raise ValueError('file %s must not contain chars (tabs, form-feed,...): %s' % (filename, bad_chars))
