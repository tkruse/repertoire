# Transpilers

The role of transpilers is to take song files in songmarkdown format (roughly what ultimate guitar files usually look like) into tex files.
Each folder needs to have a `variant.mk` file with a rule to make one `.tex` file for each `.txt` file in the project sources.

Like this:

```
$(BUILD_DIR)/<transpilername>/tex/%.tex: $(TXT_DIR)/%.txt $(BUILD_DIR)/<transpilername>/
    ...
```

Several variables can be used:

* `$(TXT_DIR)` the folder having all txt source files
* `$(SOURCE_TXT)` a list of source `.txt` files
