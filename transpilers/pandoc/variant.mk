# This transpiler is now mostly obsolete and will not be maintained.



# Pandoc style: This transforms song txt files by making multiple text replacements:
# 1. changing them to simple markdown first
# 2. and then using pandoc to create .tex files.
# 3. modify the generated tex for further customiozations
#
# The songs will be rendered with a monospaced font.
# For 2-column layout, use metadata (filename) to detect.

# INstallation of pandoc required
# Each song (except title) becomes a single verbatim block, the rendering output is very predictable (but a bit boring).
# The process relies on a certain pandoc output, this is not as robust as skipping pandoc.


# make magic not needed
export MAKEFLAGS += --no-builtin-rules
.SUFFIXES:

# txt to markdown
export VARIANT_BUILD_DIR = $(BUILD_DIR)/pandoc
export MD_DIR=$(VARIANT_BUILD_DIR)/md


export VARIANT_TEX_DIR=$(VARIANT_BUILD_DIR)/tex
export TEX_2COL_DIR=$(VARIANT_BUILD_DIR)/2col_tex
export RAWTEX_DIR=$(VARIANT_BUILD_DIR)/rawtex
export INDEXED_TEX_DIR=$(VARIANT_BUILD_DIR)/indexed_tex


TRANSFORMED_MD=\
 $(subst $(TXT_DIR)/,$(MD_DIR)/,$(SOURCE_TXT:.txt=.md))

EXPORTED_RAWTEX=\
 $(subst $(MD_DIR)/,$(RAWTEX_DIR)/,$(TRANSFORMED_MD:.md=.tex))

EXPORTED_INDEXED_TEX=\
 $(subst $(MD_DIR)/,$(INDEXED_TEX_DIR)/,$(TRANSFORMED_MD:.md=.tex))


EXPORTED_TEX_2COL=\
 $(subst $(MD_DIR)/,$(TEX_2COL_DIR)/,$(TRANSFORMED_MD:.md=.tex))
# keep files in build folder, so that next call to make can reuse them
.SECONDARY: $(EXPORTED_INDEXED_TEX) $(EXPORTED_RAWTEX) $(EXPORTED_TEX_2COL) $(TRANSFORMED_MD)

#################### TXT to Markdown

# transform plain txt files to markdown just indenting the song body
$(MD_DIR)/%.md: $(TXT_DIR)/%.txt $(MD_DIR)/
	sed 's/^[^#:]/    &/g' $< > $(subst .txt,.md,$@)


#################### Pandoc to transform MD to TEX

PANDOC_TEMPLATE=$(VARIANTS_DIR)/pandoc/pandoc/pandoc_template.tex

PANDOC=pandoc
PANDOC_OPTIONS=-f markdown+smart --standalone --template=$(PANDOC_TEMPLATE)
PANDOC_TEX_OPTIONS=--to latex

$(TEX_2COL_DIR)/%_2col.tex: $(MD_DIR)/%_2col.md $(TEX_2COL_DIR)/ $(PANDOC_TEMPLATE)
	echo 'writing to -o $@'
	$(PANDOC) $(PANDOC_OPTIONS) $(PANDOC_TEX_OPTIONS) -o $@ $<

# deprioritized rule for all non-2col documents
$(RAWTEX_DIR)/%.tex: $(MD_DIR)/%.md $(RAWTEX_DIR)/ $(PANDOC_TEMPLATE) | dummy% dummy%x
	$(PANDOC) $(PANDOC_OPTIONS) $(PANDOC_TEX_OPTIONS) -o $@ $<

#################### Handle 2-column files

# for 2column layout, add multicol directives, and clear page before
$(RAWTEX_DIR)/%_2col.tex: $(TEX_2COL_DIR)/%_2col.tex $(RAWTEX_DIR)/
	sed 's;\\begin{verbatim};\\begin{multicols}{2}\n\\begin{verbatim};g' $< \
	| sed 's;\\end{verbatim};\\end{verbatim}\n\\end{multicols};g' \
	| sed 's;\\section;\\clearpage{}\n\\section;g' \
	> $(subst .txt,.md,$@)

#################### Add indexing

# Include raw tex files if needed
export OVERRIDE_DIR=$(TEX_SRC_DIR)/override
OVERRIDE_TEX := $(wildcard $(OVERRIDE_DIR)/*.tex)
$(INDEXED_TEX_DIR)/%.tex: $(OVERRIDE_DIR)/%.tex $(INDEXED_TEX_DIR)/
	cp $< $@


# Add indexing for song titles "\section{foo - bar}\index{foo}
$(INDEXED_TEX_DIR)/%.tex: $(RAWTEX_DIR)/%.tex $(INDEXED_TEX_DIR)/ | dummy% dummy%x
	perl -0777 -pe 's/(\\section\{[\n]+)\n/\1/igs' $< \
  | perl -0777 -pe 's/\\section\{(([^}]+) - [^}]+)\}/\\section\{\1\}\\index\{\2\}/igs' \
  > $@

#################### Other custom preprocessing

$(BUILD_DIR)/pandoc/tex/%.tex: $(INDEXED_TEX_DIR)/%.tex $(BUILD_DIR)/pandoc/tex/
	cp $< $@

# this song needs smaller fonts to fit the page, using Verbatim latex environment
$(BUILD_DIR)/pandoc/tex/en1_dylan_hurricane_2col.tex: $(INDEXED_TEX_DIR)/en1_dylan_hurricane_2col.tex
	sed 's;\\begin{verbatim};\\begin{Verbatim}[fontsize=\\footnotesize];g' $< \
	| sed 's;\\end{verbatim};\\end{Verbatim};g' \
	> $@

