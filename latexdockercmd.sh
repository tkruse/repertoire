#!/bin/sh

# rebuild image from Dockerfile. This can take a few minutes the first time.
# docker build -t leadsheets docker

# run command
exec docker run --rm -i  --name leadsheets --user="$(id -u):$(id -g)" --net=none -v "$PWD":/data -d leadsheets "$@"
