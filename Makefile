# make magic not needed
export MAKEFLAGS += --no-builtin-rules
.SUFFIXES:

# only works when running make in root folder :-(
export ROOT_DIR=$(shell pwd)

# parent folder for all transient files
export BUILD_DIR=$(ROOT_DIR)/build


# txt sources
export TXT_DIR=$(ROOT_DIR)/txt
SOURCE_TXT := $(wildcard $(TXT_DIR)/*.txt)

# static sources
export TEX_SRC_DIR=$(ROOT_DIR)/tex

# deprioritize rules by depending on this
.PHONY: dummy%
dummy%:
	@:

.PHONY: print-%
# Debug any of the variables
print-%: ; @echo $* = $($*)


##################### Load targets from variant

export VARIANTS_DIR=$(ROOT_DIR)/transpilers

# simple python file converting to simple latex
include $(VARIANTS_DIR)/plain/variant.mk
# convert txt to markdown then via pandoc to simple latex
include $(VARIANTS_DIR)/pandoc/variant.mk
# use custom parser to write complex latex
include $(VARIANTS_DIR)/leadsheets/variant.mk

###################### Create Folders

.PRECIOUS: $(BUILD_DIR)/
$(BUILD_DIR)/:
	mkdir -p $@

.PRECIOUS: $(BUILD_DIR)/%/
$(BUILD_DIR)/%/:
	mkdir -p $@

.PRECIOUS: $(BUILD_DIR)/%/tex/
$(BUILD_DIR)/%/tex/:
	mkdir -p $@

.PRECIOUS: $(BUILD_DIR)/%/tmp/
$(BUILD_DIR)/%/tmp/:
	mkdir -p $@

#######################  Clean command

.PHONY: clean
clean:
	rm -rf build
	rm -rf dist


#######################  PDF generation via tex

export SUPPORT_SRC := $(wildcard $(TEX_SRC_DIR)/*.tex) $(TEX_SRC_DIR)/index_style.ist

# cannot use $(BUILD_DIR) for jobname
export PDF_TMP_JOBNAME=tmp/songbook
export PDF_TMP_FILENAME=$(PDF_TMP_JOBNAME).pdf

export DIST_DIR=$(ROOT_DIR)/dist
export PDF_FILENAME=songbook.pdf

.PRECIOUS: $(BUILD_DIR)/%/variant_packages.tex
$(BUILD_DIR)/%/variant_packages.tex: $(VARIANTS_DIR)/%/tex/variant_packages.tex
	@mkdir -p $(BUILD_DIR)/$*
	cp $(VARIANTS_DIR)/$*/tex/*.tex $(BUILD_DIR)/$*

.PRECIOUS: $(BUILD_DIR)/%/document.tex
$(BUILD_DIR)/%/document.tex: $(SUPPORT_SRC)
	@mkdir -p $(BUILD_DIR)/$*
	cp $(TEX_SRC_DIR)/* $(BUILD_DIR)/$*

# helper tex file to be generated including all transpiled tex files.
export INPUTS_TEX_FILENAME=inputs.tex

.PRECIOUS: $(BUILD_DIR)/%/$(INPUTS_TEX_FILENAME)
$(BUILD_DIR)/%/$(INPUTS_TEX_FILENAME): $(BUILD_DIR)/%/tex/  $(subst $(TXT_DIR)/,$(BUILD_DIR)/%/tex/,$(SOURCE_TXT:.txt=.tex))
# in inputs.tex, include all generated files with a newpage directive between them
	ls $(BUILD_DIR)/$*/tex/*.tex | awk '{printf "\\input{%s}\n", $$1}' > $(BUILD_DIR)/$*/$(INPUTS_TEX_FILENAME)


.PRECIOUS: $(BUILD_DIR)/%/tmp/$(PDF_FILENAME)
$(BUILD_DIR)/%/tmp/$(PDF_FILENAME): $(BUILD_DIR)/%/tmp/ $(BUILD_DIR)/%/$(INPUTS_TEX_FILENAME) $(BUILD_DIR)/%/variant_packages.tex  $(BUILD_DIR)/%/document.tex
	cd $(BUILD_DIR)/$*; latexmk -xelatex -interaction=nonstopmode -jobname=$(PDF_TMP_JOBNAME) -pdfxe $(BUILD_DIR)/$*/document.tex


.PRECIOUS: $(DIST_DIR)/
$(DIST_DIR)/:
	@mkdir -p $(DIST_DIR)

.PRECIOUS: $(DIST_DIR)/%/
$(DIST_DIR)/%/:  $(DIST_DIR)/
	@mkdir -p $@

.PRECIOUS: $(DIST_DIR)/%/$(PDF_FILENAME)
$(DIST_DIR)/%/$(PDF_FILENAME): $(DIST_DIR)/%/ $(BUILD_DIR)/%/tmp/$(PDF_FILENAME)
	cp $(BUILD_DIR)/$*/tmp/$(PDF_FILENAME) $(DIST_DIR)/$*/$(PDF_FILENAME)

######################### default make result

# Alternative make targets called like "make pdf-leadsheets"
.PHONY: pdf-%
pdf-%: $(DIST_DIR)/%/$(PDF_FILENAME)
	@echo Done generating

.DEFAULT_GOAL := default
.PHONY: default
default: pdf-plain

